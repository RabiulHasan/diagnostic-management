﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DMS.Models;
using System.Net;
using PagedList;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Data.SqlClient;
using System.Data;
namespace DMS.Controllers
{
    public class SettingsController : Controller
    {
        private DiagnosticMS_DBContext db = new DiagnosticMS_DBContext();
        //
        // GET: /Settings/
        public ActionResult Index()
        {
            ///*Getting data from database*/
            //List<codes_type> objtypelist = (from data in db.codes_type
            //                                   select data).ToList();
            //codes_type objType = new codes_type();
            //objType.type = "Select";
            //objType.com_code = 0;
            //objtypelist.Insert(0, objType);
            //SelectList objmodeldata = new SelectList(objtypelist, "com_code", "type", 0);

            ///*Assign value to model*/
            //CodesType objCodesType = new CodesType();
            //objCodesType.CodesTypeList = objmodeldata;
            //return View(objCodesType);
            return View();
        }
        [HttpGet]
        public ActionResult GetCategory()
        {
            var categoryList = db.codes_type.ToList();
            return View(categoryList);
        }
        public ActionResult Category()
        {
            
            return View();
        }
        [HttpPost]
        
        public ActionResult Category(codes_type categories)
        {
            if (ModelState.IsValid)
            {
                categories.com_code = Convert.ToInt32(Session["ComCode"]);
                db.codes_type.Add(categories);
                db.SaveChanges();
                ViewBag.Mesage =string.Format("Saved Success");

            }
     
            return View(categories);
            
        }
        public ActionResult SubCategory()
        {
            var ListData = db.codes_type.ToList();
            ViewBag.List = new SelectList(ListData, "type", "type", "0");
            return View();

            /*Getting data from database*/
            //List<codes_type> objtypelist = db.codes_type.ToList();
            //codes_type objType = new codes_type();
            //objType.type = "--Select--";
            //objtypelist.Insert(0, objType);
            //SelectList objmodeldata = new SelectList(objtypelist, "type", "type", 0);

            /*Assign value to model*/
            //code objCodesType = new code();
            //objCodesType.CodesTypeList = objmodeldata;

            //return View(objCodesType);
           
        }

        [HttpPost]
        public ActionResult SubCategory(code subcatecogry)
        {
            if (ModelState.IsValid)
            {
                subcatecogry.com_code = Convert.ToInt32(Session["ComCode"]);
                subcatecogry.entered_by = Session["UserId"].ToString();
                db.codes.Add(subcatecogry);
                db.SaveChanges();
                return RedirectToAction("getSubcategory");
                //ViewBag.Message = string.Format("Saved Successfully");
            }
            else
            {
                ViewBag.Message = string.Format("Failed");
            }
            return View();
        }
        [HttpGet]
        public ActionResult getSubcategory()
        {
           // db.Configuration.ProxyCreationEnabled = false;
            //var codeslist = db.codes.ToList<code>();
            //return Json(new { data = codeslist }, JsonRequestBehavior.AllowGet);
            var data = db.codes.ToList();
            return View(data);
        }
        public ViewResult getSubcategory(string searchString)
        {
            var code = from c in db.codes
                           select c;
            if (!String.IsNullOrEmpty(searchString))
            {
                code = code.Where(c => c.code1.Contains(searchString));
            }
           
            return View(code.ToList());
        }

        public ActionResult DeleteSubCategory(string id)
        {
            try
            {
                code subcategory = db.codes.Find(id);
                db.codes.Remove(subcategory);
                db.SaveChanges();
            }
            catch (Exception)
            {
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });  throw;
            }
            return RedirectToAction("getSubcategory");
        }
        public ActionResult registered()
        {
            var ListCompany = db.com_info.ToList();
            if(ListCompany!=null)
            {
                ViewBag.List = new SelectList(ListCompany, "com_code", "com_name", 0);
            }

            return View();
        }
        [HttpPost]
        public ActionResult registered(user_info users)
        {
            if (ModelState.IsValid)
            {
                db.user_info.Add(users);               
                db.SaveChanges();
                ViewBag.Message = "Saved Success";
                return RedirectToAction("GetUserInfo", "Settings");

            }
            else
            {
                ViewBag.Message = "Failed";
            }

            return View(users);
        }
        [HttpGet]
        public ActionResult GetUserInfo()
        {
            var data = db.user_info.ToList();

            return View(data);
        }
        public ActionResult DeleteUser(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user_info user_info = db.user_info.Find(id);
            if (user_info == null)
            {
                return HttpNotFound();
            }
            return View(userInfo);
        }
        //[HttpPost, ActionName("DeleteUser")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            user_info user_info = db.user_info.Find(id);
            db.user_info.Remove(user_info);
            db.SaveChanges();
            return RedirectToAction("GetUserInfo");
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public IView userInfo { get; set; }

        public ActionResult ShowTestList()
        {
           // var testList = (from t in db.codes select t).ToList();
   
            var testList = db.codes.ToList();
         
            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports/rpt_GetAllTest.rpt")));

            rd.SetDataSource(testList);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();


            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf","list.pdf");
        }
    }
}