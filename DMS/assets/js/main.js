+function ($) {
    "use strict";

    $(document).ready(function ($) {

        
    
        if(!is_touch_device() && $('.navbar-toggle:hidden')){
          $('.dropdown-menu', this).css('margin-top',0);
          $('.dropdown').hover(function(){ 
              $('.dropdown-toggle', this).trigger('click');
              //uncomment below to make the parent item clickable
              //$('.dropdown-toggle', this).toggleClass("disabled"); 
          });			
        }   

    });

    $(window).on('load', function () {
    });
}(jQuery);

function is_touch_device() {
    return 'ontouchstart' in window        // works on most browsers 
    || navigator.maxTouchPoints;       // works on IE10/11 and Surface
};