﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using DMS.Models;
using System.Text;
namespace DMS.Controllers
{
    public class DiagnosticController : Controller
    {
        //
        // GET: /Diagnostic/
        private DiagnosticMS_DBContext db = new DiagnosticMS_DBContext();

        public ActionResult Index()
        {

            return View();
        }
        //[HttpPost]
        //    public ActionResult Index(CodeModel model)
        //    {
        //        var selectCodes = model.codes.Where(x => x.IsCheked == true).ToList<code>();
        //        return Content(string.Join("-", selectCodes.Select(x => x.code1)));
        //    }

        [HttpGet]
        public ActionResult Invoice()
        {
            customer_info cusInfo = new customer_info();

            cusInfo.doctors = db.doctors_info.ToList<doctors_info>();
            cusInfo.codes = db.codes.ToList<code>();


            return View(cusInfo);
        }

        [HttpPost]
        public ActionResult Invoice(customer_info customers)
        {
            if ((string)Session["ComCode"] != null && (string)Session["UserId"] != null)
            {

               // StringBuilder sb = new StringBuilder();
                //code cod = new code();

                foreach (var item in customers.codes)
                {
                   if (item.IsCheked)
                   {
                        //sb.Append(item.code1 + ",");

                        //var quan = db.codes.Where(x => x.code1 == item.code1).FirstOrDefault();
                        //if (quan != null)
                        //{
                        //    ViewBag.testname = quan.code1;
                        //    ViewBag.price = quan.test_price;
                            

                      //  }
                      // ViewBag.testname=item.code1.ToString();
                      //ViewBag.price=item.test_price.ToString();
                        customers.com_code = (string)Session["ComCode"];
                        customers.added_by = (string)Session["UserId"];
                        //db.customer_info.Add(customers);
                        //db.SaveChanges();
                        //ViewBag.Message = string.Format("Saved Successfully");
                        //return RedirectToAction("CustomInvoice");
                    }

                   // ViewBag.Message = sb.ToString();
                }

            }
            return View(customers);
        }

        public ActionResult CustomInvoice()
        {
            return View();
        }



        //add Doctor Information
        public ActionResult doctorInfo()
        {
            return View();
        }

        [HttpPost]
        public ActionResult doctorInfo(doctors_info docInfo)
        {
            if (ModelState.IsValid)
            {
                db.doctors_info.Add(docInfo);
                db.SaveChanges();
                ViewBag.Message = string.Format("Saved Successfully");
            }
            return View(docInfo);
        }
        [HttpGet]
        public ActionResult GetDoctorList()
        {
            var data = db.doctors_info.ToList();
            return View(data);

        }

        public ActionResult InsertCustomer(customer_info cus)
        {
            customer_info customer = new customer_info()
            {
                invoice_no = cus.invoice_no,
                customer_name = cus.customer_name

            };
            db.customer_info.Add(customer);
            db.SaveChanges();
            return View();


        }

        public ActionResult InsertTable(test_tbl tbl)
        {
            test_tbl tbls = new test_tbl()
            {
                name = tbl.name,
                department = tbl.department


            };
            db.test_tbl.Add(tbls);
            db.SaveChanges();
            return View(tbls);
        }


    }



}

