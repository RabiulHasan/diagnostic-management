﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DMS.Models;

namespace DMS.Controllers
{
    public class AuthenticationController : Controller
    {
        private DiagnosticMS_DBContext db = new DiagnosticMS_DBContext();
        //
        // GET: /Authentication/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SignIn()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SignIn(user_info login)
        {
            var admin = db.user_info.Where(m => m.user_id == login.user_id && m.password == login.password && m.user_type == "Administrator").FirstOrDefault();
            var operators = db.user_info.Where(m => m.user_id == login.user_id && m.password == login.password && m.user_type == "operator").FirstOrDefault();
            if (operators != null)
            {
                Session["ComCode"] = operators.com_code.ToString();
                Session["UserId"] = operators.user_id.ToString();
                Session["OpFullName"] = operators.full_name.ToString();
                ViewBag.UserName = operators.full_name.ToString();
                return RedirectToAction("Invoice", "Diagnostic");
            }
            if (admin != null)
            {
                Session["ComCode"] = admin.com_code.ToString();
                Session["UserId"] = admin.user_id.ToString();
                Session["AdFullName"] = admin.full_name.ToString();
                Session["EntryDate"] = admin.entry_date.ToString();
                ViewBag.UserName = admin.full_name.ToString();
                return RedirectToAction("Module", "Authentication");
            }
            else
            {
                ViewBag.Message = string.Format("UserName and Password is incorrect");
                return View();
            }



        }
        public ActionResult Module()
        {
            return View();
        }
        [HttpGet]
        public ActionResult GetCompanyList()
        {
           var GetList=db.com_info.ToList<com_info>();
           return View(GetList);
        }

        public ActionResult CompanyInfo()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CompanyInfo(com_info comInfo)
        {
            if (ModelState.IsValid)
            {
                db.com_info.Add(comInfo);
                db.SaveChanges();
                ViewBag.Message = string.Format("Saved Succesfully.");
            }
            else 
            {
                ViewBag.Message = string.Format("Data Is not Saved");
            }
            return View();

        }
     
    }
}