﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using DMS.Models;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace DMS.Controllers
{
    public class ReportController : Controller
    {
        //
        // GET: /Report/
        DiagnosticMS_DBContext db = new DiagnosticMS_DBContext();
        public ActionResult Lists()
        {
            var testList = db.test_tbl.ToList();
            return View(testList);
        }
        public ActionResult ShowTestReports()
        {

            var testList = db.test_tbl.ToList();
            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "testReport.rpt"));

            rd.SetDataSource(testList);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();


            Stream stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "CustomerList.pdf");



        }
        //public FileResult GetReport()
        //{
        //    string ReportURL = "{Your File Path}";
        //    byte[] FileBytes = System.IO.File.ReadAllBytes(ReportURL);
        //    return File(FileBytes, "application/pdf");  


        //}

	}
}