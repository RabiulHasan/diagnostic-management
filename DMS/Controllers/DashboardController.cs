﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DMS.Models;

namespace DMS.Controllers
{
    public class DashboardController : Controller
    {
        //
        // GET: /Dashboard/
        public ActionResult Index()
        {
            if ((string)Session["ComCode"] != null && (string)Session["UserId"] != null)
            {
                //ViewBag.Message = string.Format("Ok");

            }
            else {
                return RedirectToAction("SignIn","Authentication");
            }
            return View();
        }

	}
}