﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DMS.Models;

namespace DMS.Controllers
{
    public class testController : Controller
    {
        //
        // GET: /test/
        private DiagnosticMS_DBContext db = new DiagnosticMS_DBContext();
        public ActionResult Index()
        {

            return View();
        }
        public ActionResult JsonDataTable()
        {
            return View();
        }

        public JsonResult GetTestTable()
        {
            //db.Configuration.ProxyCreationEnabled = false;
            var testdata = db.test_tbl.ToList<test_tbl>();
            return Json(new {data=testdata},JsonRequestBehavior.AllowGet);
        
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }


}
